var express = require('express');
var router = express.Router();
var seasonEpi_dal = require('../model/seasonEpi_dal');

router.get('/all', function(req, res) {
    /* The /all route gets data from the account_dal.getAll()
     method and then render the companyViewAll ejs template */
    seasonEpi_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('seasonsAndEpisodes/seasonEpisodeViewAll', { result:result , title:'Seasons and Episodes', was_successful:false});
        }
    });

});

router.get('/insertSeason', function(req, res){
    // simple validation

        seasonEpi_dal.insertSeason(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                seasonEpi_dal.getAll(function(err, result){
                    if(err) {
                        res.send(err);
                    }
                    else {
                        res.render('seasonsAndEpisodes/seasonEpisodeViewAll', { result:result , title:'Seasons and Episodes', was_successful:true});
                    }
                });
            }
        });
});

router.get('/addEpisode', function(req, res){
    // passing all the query parameters (req.query) to the insert
    // function instead of each individually
    seasonEpi_dal.getAllSeasons(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('seasonsAndEpisodes/episodeAdd', {seasons: result, title:'Adding New Episode'});
        }
    });
});

router.get('/insertEpisode', function(req, res){
    // simple validation
    if(req.query.episode_name == null) {
        res.send('Episode Name must be provided.');
    }
    else if( req.query.plot == null){
        res.send('Plot must be provided.')
    }
    else if( req.query.episode_number == null){
        res.send('Episode Number must be provided.')
    }
    else if( req.query.season_number == null){
        res.send('Season Number must be provided.')
    }
    else {
        // passing all the query parameters (req.query) to the insert function
        // instead of each individually
        /* The insert function creates an INSERT SQL query string with two
         questions marks representing placeholders for where the actual values
         will be entered.*/
        seasonEpi_dal.insertEpisode(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                seasonEpi_dal.getAll(function(err, result){
                    if(err) {
                        res.send(err);
                    }
                    else {
                        res.render('seasonsAndEpisodes/seasonEpisodeViewAll', { result:result , title:'Seasons and Episodes', was_successful:true});
                    }
                });
            }
        });
    }
});

module.exports = router;