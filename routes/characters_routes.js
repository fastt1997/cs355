var express = require('express');
var router = express.Router();
var characters_dal = require('../model/character_dal');

router.get('/all', function(req, res) {
    /* The /all route gets data from the account_dal.getAll()
     method and then render the companyViewAll ejs template */
    characters_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('characters/charactersViewAll', { result:result , title:'Characters', was_successful:false});
        }
    });
});

router.get('/success', function(req, res, next) {
    res.render('success', { title: 'Success' });
});

router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert
    // function instead of each individually
    characters_dal.getShit(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('characters/charactersAdd', {actors: result[0], universe:result[1], planet:result[2], title:'Adding New Character'});
        }
    });
});

router.get('/insert', function(req, res){
    // simple validation
    if(req.query.character_full_name == null) {
        res.send('Actor Name must be provided.');
    }
    else if( req.query.age == null){
        res.send('Age must be provided.')
    }
    else if( req.query.biography == null){
        res.send('Gender must be provided.')
    }
    else if( req.query.species == null){
        res.send('Image URL must be provided.')
    }
    else if( req.query.actor_name == null){
        res.send('Image URL must be provided.')
    }
    else if( req.query.universe_name == null){
        res.send('Image URL must be provided.')
    }
    else if( req.query.appearance == null){
        res.send('Image URL must be provided.')
    }
    else if( req.query.personality == null){
        res.send('Image URL must be provided.')
    }
    else {
        // passing all the query parameters (req.query) to the insert function
        // instead of each individually
        /* The insert function creates an INSERT SQL query string with two
         questions marks representing placeholders for where the actual values
         will be entered.*/
        characters_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                characters_dal.getAll(function(err, result){
                    if(err) {
                        res.send(err);
                    }
                    else {
                        res.render('characters/charactersViewAll', { result:result , title:'Characters', was_successful:true});
                    }
                });
            }
        });
    }
});

module.exports = router;