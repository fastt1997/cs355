var express = require('express');
var router = express.Router();
var actor_dal = require('../model/actor_dal');

router.get('/all', function(req, res) {
    /* The /all route gets data from the account_dal.getAll()
     method and then render the companyViewAll ejs template */
    actor_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('actor/actorViewAll', { result:result ,title:'Actors', was_successful:false});
        }
    });

});

router.get('/success', function(req, res, next) {
    res.render('success', { title: 'Success' });
});

router.get('/', function(req, res){
    if(req.query.actor_name == null) {
        res.send('Please select an actor');
    }
    else {
        actor_dal.getVoicedChar(req.query.actor_name, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('actor/voicedCharacters', {'result': result, title:'Actors'});
            }
        });
    }
});

// Return the add a new company form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert
    // function instead of each individually
    actor_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('actor/actorAdd', {actor: result, title:'Add New Voice Actors'});
        }
    });
});

router.get('/insert', function(req, res){
    // simple validation
    if(req.query.actor_name == null) {
        res.send('Actor Name must be provided.');
    }
    else if( req.query.age == null){
        res.send('Age must be provided.')
    }
    else if( req.query.gender == null){
        res.send('Gender must be provided.')
    }
    else if( req.query.img == null){
        res.send('Image URL must be provided.')
    }
    else {
        // passing all the query parameters (req.query) to the insert function
        // instead of each individually
        /* The insert function creates an INSERT SQL query string with two
         questions marks representing placeholders for where the actual values
         will be entered.*/
        actor_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                actor_dal.getAll(function(err, result){
                    if(err) {
                        res.send(err);
                    }
                    else {
                        res.render('actor/actorViewAll', { result:result ,title:'Actors', was_successful:true});
                    }
                });
            }
        });
    }
});
module.exports = router;