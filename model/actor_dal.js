var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'select * from Voice_Actor;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getVoicedChar = function(actor_name, callback) {
    var query = 'call actor_getChars(?)';
    var queryData = [actor_name];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE COMPANY
    var query = 'INSERT INTO Voice_Actor (actor_name, age, gender, img) VALUES (?,?,?,?);';

    var queryData = [params.actor_name, params.age, params.gender, params.img];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};