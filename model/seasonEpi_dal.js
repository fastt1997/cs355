var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'select s.*, e.* from Season s ' +
                'join Season_Airs_Episode sae on s.season_number = sae.season_number ' +
                'join Episode e on sae.episode_name = e.episode_name ' +
                'order by e.episode_number, s.season_number asc;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getAllSeasons = function(callback) {
    var query = 'select * from Season';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.insertEpisode = function(params, callback) {

    var query = 'INSERT INTO Episode(episode_name, episode_number, plot) VALUES (?,?,?);';

    var queryData = [params.episode_name, params.episode_number, params.plot];

    connection.query(query, queryData, function(err, result) {

        var query = 'INSERT INTO Season_Airs_Episode (season_number, episode_name) VALUES (?,?);';

        var queryData2 = [params.season_number, params.episode_name];
        connection.query(query, queryData2, function(err, result){
            callback(err, result);
        });
    });

};

exports.insertSeason = function(params, callback) {
    var query = 'insert into Season() value();';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};